This program concatenates all the files provided and creates a .mobi ebook.

Usage: ./bookbinder -t title file(s)

If you want to include all the files in a folder, or all files with an specific extension, and any of those files include an space, you will have to use quotes. For example: ./bookbinder -t Title "\*".md

It's easier to not have spaces in the filenames, though.

This program requires Calibre, because it uses ebook-convert in the back.
