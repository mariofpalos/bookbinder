#!/bin/bash
# Usage: ./bookbinder -t <title> <markdown file(s)>
# YOu can select all items of a folder with *, or *.extension

# First I check that ebook-convert is installed, or I will not be able to work.

if ! type "ebook-convert" > /dev/null; then
	echo "You need to install Calibre on this machine."
	exit 1
fi

# After that, I make sure that you provided me with a title (using -t).

tflag=false

while getopts ":t:" opt; do
  case $opt in
    t)
      title=$OPTARG
      tflag=true
      echo "Title: $title" #just for testing
      ;;
    \?) #if the wrong flag is provided
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :) #in case -t is included with nothing after it
      echo "Option -$OPTARG requires a title." >&2
      exit 1
      ;;
  esac
done

if [ $tflag = false ]; then
   echo "You need to give me a title for your book. Use -t."
   exit 1	
fi 

# Now I transform your title into a filename-looking filename

filename=${title,,}		# by writing it on lowercase
filename=${filename// /_} 	# and substituting spaces for underscores
echo "Filename: $filename.md"

# Optional: I can add the current date to the title

date="$(date +"%d-%m-%y %H:%M")"
title="$title ($date)" 

# Next I concatenate whatever text files you gave me.
# What if I put the temporary files in /tmp/?

files=${@:3}			# this means all arguments starting from the third.
echo "Files: $files"
cat $files > $filename.md 	

#I'm diving deeper in ebook-convert options. Trying:
#--insert-blank-line -> doesn't do what I thought it would.
#--disable-markup-chapter-headings -> No effect, but maybe it will help for the TOC.
#--use-auto-toc -> Nope.
#--chapter //h:h1 -> Works!

#add --cover someday.

# And now with everything ready I create your electronic book.

ebook-convert $filename.md ~/Dropbox/Textos/Mobis/$filename.mobi --authors 'Mario F. Palos' --title "$title" --chapter //h:h1

#Done. Just some cleanup left to do.

rm -v $filename.md
